//worked with study group Elisia Burt, Mai Nguyen, T.L. Williams & Steve Carrington
//in facilitator Kano Marvel's study group to demo/go through the assessment
//assistance from tutor Gabby Basha


import React, { Component } from "react";
import todosList from "./todos.json";
import { v4 as uuidv4 } from "uuid";

class App extends Component {
  state = {
    todos: todosList,
    value: ""
  };

  handleChange = (event) => {
    this.setState({ value: event.target.value });
    
  }

  handleAddTodo = (event) => {
    //console.log("handling Adding Todo")
    const newTodo = {
      "userId": 1,
      "id": uuidv4(),
      "title": this.state.value,
      "completed": false
    }
    const newTodos = [...this.state.todos, newTodo]
    this.setState({ todos: newTodos })
    event.target.value = ""
    console.log(event)
    console.log(this.state.value)

    this.setState({ value: " " })
  }

  handleSubmit = (event) => {
    if (event.key === "Enter") {
      console.log(event.target.value)
      this.handleAddTodo(event)
    }
  }

  handleDelete = (todoId) => {
    const newTodos = this.state.todos.filter(
      todoItem => todoItem.id !== todoId
    )
    this.setState({ todos: newTodos })
  }

  handleCheck = (checkId) => {
    const newTodos = this.state.todos.map(
      todoItem => {
        if (todoItem.id === checkId) {
          todoItem.completed = !todoItem.completed
        }
        return todoItem
      }
    )
    this.setState({ todos: newTodos })
  }

  handleDeleteComplete = () => {
    // console.log("delecompleted hit")
    const newTodos = this.state.todos.filter(
      todoItem => todoItem.completed !== true
    )
    this.setState({ todos: newTodos })
  }


  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            onKeyDown={this.handleSubmit.bind(this)}
            type="text"
            onChange={this.handleChange}
            value={this.state.value}
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
          />
        </header>
        <TodoList
          todos={this.state.todos}
          handleCheck={this.handleCheck}
          handleDelete={this.handleDelete} />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button
            className="clear-completed"
            onClick={this.handleDeleteComplete}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              key={todo.id}
              handleCheck={this.props.handleCheck}
              handleDelete={this.props.handleDelete}
              title={todo.title}
              completed={todo.completed}
              id={todo.id} />
          ))}
        </ul>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            onChange={() => this.props.handleCheck(this.props.id)}
            className="toggle"
            type="checkbox"
            checked={this.props.completed} />

          <label>{this.props.title}</label>

          <button
            className="destroy"
            onClick={() => this.props.handleDelete(this.props.id)}
          />
        </div>
      </li>
    );
  }
}


export default App;
